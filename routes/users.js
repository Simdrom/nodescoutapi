var express = require('express'),
  router = express.Router(),
  User = require('../models/Users.js'),
  bcrypt = require('bcrypt'),
  uuid = require('uuid'),
  nJwt = require('njwt');

router.get('/', function (req, res, next) {
  User.find(function (err, user) {
    if (err) return next(err);
    res.json(user);
  });
});

router.get('/:username&:password', function (req, res, next) {
  User.findOne({username: req.params.username}, function (err, user) {
    if (err) return next(err);
    let passwordIsValid = bcrypt.compareSync(req.params.password, user.password);
    if (!passwordIsValid) return res.status(401).send({"authorize":false});

    res.json(user);
  });
});
router.post('/login', function (req, res) {
  User.findOne({ username: req.body.username }, function (err, user) {
    if (err)
      return res.status(500).send('Error on the server.');
    if (!user) return res.json("No user found.");
    // if (err) return next(err);
    let passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) return res.status(401).send({"authorize":false});
    const secretKey = uuid.v4();

    const token = nJwt.create(user, secretKey);

    const userWithToken = JSON.parse(JSON.stringify(user));
    delete userWithToken['password'];
    userWithToken.token=token.compact();

    res.json(userWithToken);
  });
});

router.post('/', function (req, res, next) {
  User.create(req.body, function (err, user) {
    if (err) return next(err);
    res.json(user);
  });
});

router.delete('/:id', function (req, res, next) {
  User.findByIdAndRemove(req.params.id, req.body, function (err, resource) {
    if (err) return next(err);
    res.json(resource);
  });
});
module.exports = router;
