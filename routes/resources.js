var express = require('express');
var router = express.Router();
var Resource = require('../models/Resources.js');


/* GET ALL BOOKS */
router.get('/', function (req, res, next) {
  Resource.find(function (err, resource) {
    if (err) return next(err);
    res.json(resource);
  });
});

/* GET SINGLE BOOK BY ID */
router.get('/:id', function (req, res, next) {
  Resource.findById(req.params.id, function (err, resource) {
    if (err) return next(err);
    res.json(resource);
  });
});

/* SAVE BOOK */
router.post('/', function (req, res, next) {
  Resource.create(req.body, function (err, resource) {
    if (err) return next(err);
    res.json(resource);
  });
});

/* UPDATE BOOK */
router.put('/:id', function (req, res, next) {
  Resource.findByIdAndUpdate(req.params.id, req.body, function (err, resource) {
    if (err) return next(err);
    res.json(resource);
  });
});

/* DELETE BOOK */
router.delete('/:id', function (req, res, next) {
  Resource.findByIdAndRemove(req.params.id, req.body, function (err, resource) {
    if (err) return next(err);
    res.json(resource);
  });
});

/* DELETE ALL*/
router.delete('/', function (req, res, next) {
  Resource.find(function (err, products) {
    if (err) return next(err);
    for (var i in products)
      Resource.findByIdAndRemove(products[i]._id, req.body, function (err, resource) {});
  });
});
  module.exports = router;