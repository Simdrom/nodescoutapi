var mongoose = require('mongoose');

var ResourceSchema = new mongoose.Schema({
    name: String,
    content: String,
    author: String,
    description: String,
    updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Resource', ResourceSchema);